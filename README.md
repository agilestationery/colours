# Colours #

A little library for dealing with HTML / CSS style hexadecimal colours e.g. red -> `#FF000`.


### What is this repository for? ###

The library provides:

 * enumerations of the Agile Stationery index and task-card palettes 
 * function to calculate the hue of a single hex colour
 * getters which decompose the `R`, `G` and `B` values
 * a function to calculate the distance between two colurs in euclidean `HRGB` space
 * a function to select the nearest matching colour from an enumerated set.

### How do I get set up? ###

Add the library as a maven dependency in your POM.

You'll first need to define a reference to the jitpack repo:

        <repositories>
            <repository>
		        <id>jitpack.io</id>
		        <url>https://jitpack.io</url>
		    </repository>
	    </repositories>

Then add the dependency itself:

        <dependency>
	        <groupId>org.bitbucket.agilestationery</groupId>
	        <artifactId>colours</artifactId>
	        <version>0.0.1</version>
	    </dependency>

### Contribution guidelines ###

There are detailed guidelines for contributions yet. The only policy is "please do get in touch".

### Who do I talk to? ###

Talk to Simon at Agile Stationery 

https://agilestationery.co.uk/pages/get-in-touch
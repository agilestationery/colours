package uk.co.agilestationery.colour;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Simon
 * @since 26/07/2017
 */
public interface EnumeratedHexColour {
	String getHex();
	HexColour getColour();

	static EnumeratedHexColour pickNearestTo(final HexColour colour, List<EnumeratedHexColour> values) {
		List<EnumeratedHexColour> sortedColours = new ArrayList<>(values);
		sortedColours.sort((a, b) -> {
			double aDst, bDst;
			aDst = a.getColour().distanceFrom(colour);
			bDst = b.getColour().distanceFrom(colour);
			return Double.compare(aDst, bDst);
		});
		return sortedColours.get(0);

	}


}

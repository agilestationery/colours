package uk.co.agilestationery.colour;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author Simon
 * @since 26/07/2017
 */
public enum PastelColour implements EnumeratedHexColour {
	PASTEL_RED("#F87C82", "P 57-4 U"), // E37771
	PASTEL_GREEN("#BBDAB7", "P 145-2 U"), // B4D3A1
	PASTEL_BLUE("#7BB7E3", "P 112-12 U"), // 76ACD1
	PASTEL_ORANGE("#FFDA73", "P 7-6 U"), //FCD06B
	PASTEL_PURPLE("#C9B0CA", "P 85-10 U"); // B39CAE

	private final HexColour colour;
	private String pantone;

	PastelColour(String hex, String pantone) {
		this.colour = new HexColour(hex);
		this.pantone = pantone;
	}

	public String getHex() {
		return colour.getHex();
	}

	public HexColour getColour() {
		return colour;
	}

	public String getPantone() {
		return pantone;
	}

	public static PastelColour nearestTo(HexColour colour) {
		List<EnumeratedHexColour> options = Lists.newArrayList(PastelColour.values());
		return (PastelColour) EnumeratedHexColour.pickNearestTo(colour,options);
	}

}

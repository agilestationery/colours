package uk.co.agilestationery.colour;

import com.google.common.base.Objects;

import java.awt.Color;
import java.util.Locale;

import static java.lang.Math.abs;
import static java.lang.Math.pow;

/**
 * @author Simon
 * @since 25/07/2017
 */
public class HexColour {

	private final double hue;
	private final double saturation;
	private final double brightness;
	private String hex;

	public HexColour(String hex) {
		// #rrggbb
		if(!hex.startsWith("#")) {
			hex = "#" + hex;
		}
		if(hex.length()>8) {
			throw new IllegalArgumentException("Too many digits: " + hex);
		}
		this.hex = hex;

		float hsb[] = Color.RGBtoHSB((int) getR(),(int) getG(),(int) getB(), new float[3]);

		this.hue = hsb[0] * 360;
		this.saturation = hsb[1];
		this.brightness = hsb[2];

	}

	public short getR() {
		String rHex = hex.substring(1,3);
		return Short.parseShort(rHex,16);
	}

	public short getG() {
		String rHex = hex.substring(3,5);
		return Short.parseShort(rHex,16);
	}

	public short getB() {
		String rHex = hex.substring(5);
		return Short.parseShort(rHex,16);
	}

	public double getHue() {
		return hue;
	}

	public double getSaturation() {
		return saturation;
	}

	public double getBrightness() {
		return brightness;
	}

	public double distanceFrom(HexColour other) {
		double [] thisVector = new double[] { this.getSaturation(), this.getBrightness()};
		double [] otherVector = new double[] { other.getSaturation(), other.getBrightness() };

		double sumOfSquaredDifferences = 0;

		for(int i =0;i<thisVector.length;i++) {
			double difference = thisVector[i] - otherVector[i];
			sumOfSquaredDifferences = pow(difference, 2);
		}

		double wrapAroundHueDifference = hueDifference(other);

		sumOfSquaredDifferences += pow(wrapAroundHueDifference, 2);

		return Math.sqrt(sumOfSquaredDifferences);
	}

	public double hueDifference(HexColour other) {
		return 180d - abs(abs(other.getHue() - this.getHue()) - 180d);
	}


	public String getHex() {
		return hex.toLowerCase(Locale.UK);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof HexColour)) return false;
		HexColour colour = (HexColour) o;
		return Objects.equal(getHex(), colour.getHex());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getHex());
	}
}

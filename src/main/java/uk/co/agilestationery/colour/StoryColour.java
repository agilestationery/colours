package uk.co.agilestationery.colour;


import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author Simon
 * @since 19/07/2017
 */
public enum StoryColour implements EnumeratedHexColour {

	RED("#ED1C24"),
	GREEN("#33cc66"),
	BLUE("#0070c0"),
	ORANGE("#f9a41a"),
	PURPLE("#a74ac7");


	private final HexColour colour;

	StoryColour(String hex) {
		this.colour = new HexColour(hex);
	}

	public String getHex() {
		return colour.getHex();
	}

	public HexColour getColour() {
		return colour;
	}

	public static StoryColour nearestTo(HexColour colour) {
		List<EnumeratedHexColour> options = Lists.newArrayList(StoryColour.values());
		return (StoryColour) EnumeratedHexColour.pickNearestTo(colour,options);
	}

}

package uk.co.agilestationery.colour;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author Simon
 * @since 26/07/2017
 */
public enum TaskColour implements EnumeratedHexColour {
	LIGHT_RED("#e94a54", "PMS 032 U"),
	LIGHT_GREEN("#a0cc8a", "PMS 358 U"),
	LIGHT_BLUE("#569fd8", "PMS 292 U"),
	LIGHT_ORANGE("#ffd940", "PMS 108 U"),
	LIGHT_PURPLE("#c48dbe", "PMS 2572 U");

	private final HexColour colour;
	private String pantone;

	TaskColour(String hex, String pantone) {
		this.colour = new HexColour(hex);
		this.pantone = pantone;
	}

	public String getHex() {
		return colour.getHex();
	}

	public HexColour getColour() {
		return colour;
	}

	public String getPantone() {
		return pantone;
	}

	public static TaskColour nearestTo(HexColour colour) {
		List<EnumeratedHexColour> options = Lists.newArrayList(TaskColour.values());
		return (TaskColour) EnumeratedHexColour.pickNearestTo(colour,options);
	}

}

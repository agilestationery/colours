package uk.co.agilestationery.colour;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static uk.co.agilestationery.colour.StoryColour.*;
import static uk.co.agilestationery.colour.TaskColour.*;

/**
 * @author Simon
 * @since 25/07/2017
 */
public class HexColourTest {

	private HexColour smallExample;
	private HexColour largeExample;

	@BeforeEach
	public void setUp() {

		smallExample = new HexColour("#010203");
		largeExample = new HexColour("#102030");

	}

	@Test
	public void getR() throws Exception {
		assertEquals(1,smallExample.getR(), "low order red");
		assertEquals(16,largeExample.getR(), "high order red");
	}

	@Test
	public void getG() throws Exception {
		assertEquals(2,smallExample.getG(), "low order green");
		assertEquals(32,largeExample.getG(),"high order green");
	}

	@Test
	public void getB() throws Exception {
		assertEquals(3, smallExample.getB(),"low order blue" );
		assertEquals(48,largeExample.getB(),"high order blue");
	}

	@Test
	public void orangeHueValue() {
		assertEquals(37, ORANGE.getColour().getHue(), 1);
	}

	@Test
	public void lightOrangeOrYellowHueValue() {
		assertEquals(48, LIGHT_ORANGE.getColour().getHue(), 1);
	}

	@Test
	public void redHueValue() {
		assertEquals(357, RED.getColour().getHue(), 1);
	}

	@Test
	public void lightRedHueValue() {
		assertEquals(356, LIGHT_RED.getColour().getHue(), 1);
	}

	@Test
	void distanceBetweenRedAndOrangeIsSymmetrical() throws Exception {
		double forward = RED.getColour().distanceFrom(ORANGE.getColour());
		double backwards = ORANGE.getColour().distanceFrom(RED.getColour());

		assertEquals(forward, backwards);

	}

	@Test
	void distanceBetweenPurpleAndYellowIsSymmetrical() throws Exception {
		double forward = LIGHT_PURPLE.getColour().distanceFrom(LIGHT_ORANGE.getColour());
		double backwards = LIGHT_ORANGE.getColour().distanceFrom(LIGHT_PURPLE.getColour());

		assertEquals(forward, backwards);

	}

	@Test
	public void correctHueDifferenceBetweenTwoNearbyColours() {

		// orange 37, light orange 48

		double forward = ORANGE.getColour().hueDifference(LIGHT_ORANGE.getColour());
		assertEquals(11d, forward, 1);

		double backwards = LIGHT_ORANGE.getColour().hueDifference(ORANGE.getColour());
		assertEquals(11d, backwards, 1);
	}

	@Test
	public void correctHueDifferenceBetweenTwoColoursStraddling360Degrees() {
		// orange 37, red 358
		double forward = ORANGE.getColour().hueDifference(RED.getColour());
		assertEquals(39d, forward, 1);

		double backwards = RED.getColour().hueDifference(ORANGE.getColour());
		assertEquals(39d, backwards, 1);
	}

	@Test
	public void twoHexValuesOfDifferingCaseWillMatch() {
		HexColour lowercaseWhite = new HexColour("#ffffff");
		HexColour uppercaseWhite = new HexColour("#FFFFFF");

		assertTrue(lowercaseWhite.equals(uppercaseWhite));

	}

}
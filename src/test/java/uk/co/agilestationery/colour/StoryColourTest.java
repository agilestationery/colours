package uk.co.agilestationery.colour;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Simon
 * @since 25/07/2017
 */
public class StoryColourTest {

	@Test
	public void shouldPickNearestRedToPureRed()  {
		HexColour red = new HexColour("FF0000");
		StoryColour result = StoryColour.nearestTo(red);
		assertEquals(StoryColour.RED, result);
	}

	@Test
	public void shouldPickNearestBlueToEnhancementBlue()  {
		HexColour enhancementBlue = new HexColour("84b6eb");
		StoryColour result = StoryColour.nearestTo(enhancementBlue);
		assertEquals(StoryColour.BLUE, result);
	}

}